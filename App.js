'use strict';

import 'react-native-gesture-handler';
import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import QRScanner from './components/qrscanner';
import Product from './components/product';
import {createStackNavigator} from '@react-navigation/stack';

const Stack = createStackNavigator();

export default class App extends React.Component {
	// This is only for demo purposes with a hard coded token. In actual app, token can be acquired by the code below.
	// componentDidMount(){
	// 	var myHeaders = new Headers();
	// 	myHeaders.append("Accept", "application/json");
	// 	myHeaders.append("Content-Type", "application/json");
		
	// 	var raw = JSON.stringify({"email":"admin.hris@excelym.com","password":"Qazwsx123"});
		
	// 	var requestOptions = {
	// 	  method: 'POST',
	// 	  headers: myHeaders,
	// 	  body: raw
	// 	};
		
	// 	fetch("https://sellquant-api.excelym.com/rest/v1/Users/login", requestOptions)
	// 	  .then(response => {
	// 		  if(response.ok){
	// 				response.json().then(data => {
	// 					this.setState({token: data.id})
	// 				})
	// 				.catch(() => console.dir('Error with JSON data'))
	// 			} else {
	// 				console.dir('Error fetching data');
	// 			}
	// 	  })
	// 	  .catch(e => console.dir('Error fetching data'))
	// }

	render(){
		return (
			<NavigationContainer>
				<Stack.Navigator initialRouteName="QRScanner">
					<Stack.Screen name="QRScanner" component={QRScanner} />
					<Stack.Screen name="Product" component={Product} />
				</Stack.Navigator>
			</NavigationContainer>
		);
	}
}