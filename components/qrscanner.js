import React, { Component } from 'react';
import {Text,TouchableOpacity,Linking} from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner';
import styles from '../styles.js';

export default class QRScanner extends Component {
	constructor(props){
		super(props);
		this.scanRef = React.createRef();
	}

	onSuccess = e => {
		const {navigation} = this.props;
		navigation.navigate('Product', {
			qrData: e.data,
		});
	};

	componentDidMount(){
		this.unsubscribe = this.props.navigation.addListener('focus', () => {
			this.scanRef.current.reactivate();
		});
	}

	componentWillUnmount(){
		this.unsubscribe();
	}

  	render() {
		return (
			<QRCodeScanner
				ref={this.scanRef}
				onRead={this.onSuccess}
				// flashMode={QRCodeScanner.Constants.FlashMode.torch}
			/>
		);
	}
}