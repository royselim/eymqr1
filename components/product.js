import * as React from 'react';
import {View, Text, Image, ActivityIndicator} from 'react-native';
import styles from '../styles';

export default class Product extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			item: undefined,
			fetching: true
		}
	}

    componentDidMount(){
        const {route} = this.props;
		const {qrData} = route.params;

		// Hard coded token, in actual app, pass this down as a prop.
        const token = '8I8ZXyocXaX9hbNK5AVoahBCKqVCHS5Imnq0OMzSMvrrxKB5JKPPPSPrpzqcnd2B';
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
        };
          
        fetch(`https://sellquant-api.excelym.com/rest/v1/Items/${qrData}?access_token=${token}`, requestOptions)
		.then(response => {
			if(response.ok){
				response.json().then(item => this.setState({item, fetching: false}));
			} else {
				this.setState({fetching: false})
			}
		}).catch(() => {
			console.dir('Error fetching data');
			this.setState({fetching: false});
		});
    }

    render(){
		const {item, fetching} = this.state
        return <View style={styles.centerAll}>
            {
				fetching ?<ActivityIndicator size="large"/>:
				<View>
					{
						!item ? <Text>QR code is not recognized.</Text>: 
						<View style={styles.centerAll}>
							<View style={{flexDirection:'row'}}><Text>Product: </Text><Text>{item.description}</Text></View>
							<View style={{flexDirection:'row'}}><Text>Price: </Text><Text>{parseFloat(item.avgCostPrice).toFixed(2)}</Text></View>
							<Image source={{uri: `${item.img}`}} style={{height: 300, width: 200}}/>
						</View>
					}
				</View> 
			}
        </View>
    }
}